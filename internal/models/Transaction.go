package models

import "math/big"

type Transaction struct {
	Id              int
	HashTransaction string
}

type TransactionInfo struct {
	HashTransaction string
	From string
	To string
	Value *big.Int
	Data string
	Type string
}