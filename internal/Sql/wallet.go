package Sql

import (
	"database/sql"
)

type walletRepository struct {
	db *sql.DB
}

func (w *walletRepository) Create(address string) error {

	_, err := w.db.Exec("insert into wallets(address) values($1)", address)
	if err != nil {
		return err
	}

	return nil
}

func (w *walletRepository) FindByAddress(address string) (int, error) {

	var id int

	row := w.db.QueryRow("select id from wallets where address = $1", address)
	err := row.Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}
