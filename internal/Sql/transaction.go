package Sql

import (
	"database/sql"
	"../models"
)

type transactionRepository struct {
	db *sql.DB
}

func (t *transactionRepository) Create(id int, txHash string) error {

	_, err := t.db.Exec("insert into transactions(wallet_id, transactionHash) values($1,$2)", id, txHash)
	if err != nil {
		return err
	}

	return nil
}
func (t *transactionRepository) GetTransactions() ([]models.TransactionInfo, error) {

	rows, err := t.db.Query("select w.address, t.transactionHash from transactions t join wallets w on w.id = t.wallet_id")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var transactions []models.TransactionInfo

	for rows.Next() {
		var t models.TransactionInfo
		err = rows.Scan(&t.From, &t.HashTransaction)
		if err != nil {
			continue
		}
		transactions = append(transactions, t)
	}

	return transactions, nil
}
