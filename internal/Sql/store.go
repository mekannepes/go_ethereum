package Sql

import (
	"../store"
	"database/sql"
)

type Store struct {
	db                    *sql.DB
	walletRepository      *walletRepository
	transactionRepository *transactionRepository
}

func New(DB *sql.DB) *Store {
	s := &Store{
		db: DB,
	}
	return s
}

func (s *Store) Wallet() store.WalletRepository {
	if s.walletRepository != nil {
		return s.walletRepository
	}
	s.walletRepository = &walletRepository{
		db: s.db,
	}
	return s.walletRepository
}

func (s *Store) Transaction() store.TransactionRepository {
	if s.transactionRepository != nil {
		return s.transactionRepository
	}

	s.transactionRepository = &transactionRepository{
		db: s.db,
	}
	return s.transactionRepository
}
