package server

import (
	"../ethereum"
	"../store"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
)

type server struct {
	router   *gin.Engine
	ethereum *ethereum.Ethereum
	log      *logrus.Logger
	store    store.StoreRepositories
}

func NewServer(config *Config, store store.StoreRepositories) (*server, error) {
	s := &server{
		router: gin.New(),
		log:    logrus.New(),
	}
	var err error
	s.ethereum, err = ethereum.NewEthereum(config.EthAddr, config.keyStoreAddr)
	if err != nil {
		return nil, err
	}

	s.log.SetFormatter(&logrus.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})

	s.store = store

	s.configureRouter()
	return s, nil
}

func (s *server) configureRouter() {
	s.router.Use(cors.New(cors.Config{
		AllowMethods:     []string{"GET", "POST", "OPTIONS", "PUT", "DELETE"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "User-Agent", "Referrer", "Host", "Token", "Authorization"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowAllOrigins:  false,
		AllowOriginFunc:  func(origin string) bool { return true },
		MaxAge:           86400,
	}))
	s.router.Use(s.logger())
	api := s.router.Group("/api")
	api.POST("/create", s.create())
	api.GET("/wallets", s.wallets)
	api.GET("/balance", s.balance())
	api.POST("/transfer", s.transfer())
	api.POST("/transactions", s.transactions())
}

func (s *server) logger() gin.HandlerFunc {
	return func(c *gin.Context) {

		s.log.WithFields(logrus.Fields{
			"package":  "server",
			"function": "logger",
		}).Error("ERRORS")
		c.Next()
	}
}

func (s *server) create() gin.HandlerFunc {

	type Wallet struct {
		password string `json:"password" binding:"required"`
	}
	return func(c *gin.Context) {
		var wallet Wallet

		if err := c.ShouldBindJSON(&wallet); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		walletHex, err := s.ethereum.CreateWallet(wallet.password)
		if err != nil || len(walletHex) == 0 {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Кашелек не создан!"})
			return
		}

		err = s.store.Wallet().Create(walletHex)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Кашелек не создан!"})
			return
		}

		c.JSON(200, gin.H{"address": walletHex})

		return
	}
}

func (s *server) wallets(c *gin.Context) {
	wallets := s.ethereum.GetWallets()

	c.JSON(200, gin.H{
		"wallets": wallets,
	})
	return
}

func (s *server) balance() gin.HandlerFunc {

	type Balance struct {
		Address string `json:"address" binding:"required"`
	}

	return func(c *gin.Context) {
		var balance Balance
		if err := c.ShouldBindJSON(&balance); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		eth, err := s.ethereum.GetBalance(balance.Address)
		if err != nil {
			c.JSON(400, gin.H{"error": err.Error()})
			return
		}
		c.JSON(200, gin.H{"address": balance.Address, "balance": eth})
		return

	}
}

func (s *server) transfer() gin.HandlerFunc {

	type Transfer struct {
		From     string `json:"from" binding:"required"`
		To       string `json:"to" binding:"required"`
		Password string `json:"password" binding:"required"`
		Eth      int64  `json:"eth" binding:"required"`
	}

	return func(c *gin.Context) {

		var transfer Transfer
		if err := c.ShouldBindJSON(&transfer); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		id, err := s.store.Wallet().FindByAddress(transfer.From)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Кашелек не найден"})
			return
		}

		transferHash, err := s.ethereum.Transfer(transfer.From, transfer.To, transfer.Password, transfer.Eth)
		if err != nil || len(transferHash) == 0 {
			c.JSON(http.StatusBadRequest, gin.H{"error": "деньги не переведены"})
			return
		}

		_ = s.store.Transaction().Create(id, transferHash)

		c.JSON(200, gin.H{"transactionHash": transferHash})
		return
	}
}

func (s *server) transactions() gin.HandlerFunc {

	type Address struct {
		Address string `json:"address" binding:"required"`
	}

	return func(c *gin.Context) {

		var a Address
		if err := c.ShouldBindJSON(&a); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		transactions, err := s.store.Transaction().GetTransactions()
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		transactionsInfo, err := s.ethereum.GetLastTransactions(transactions, a.Address)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		c.JSON(200, transactionsInfo)
		return
	}
}
