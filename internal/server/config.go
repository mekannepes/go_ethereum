package server

type Config struct {
	BindAddr string
	LogLevel string
	DbURL    string
	EthAddr  string
	keyStoreAddr string
}

func NewConfig() *Config {
	c := &Config{
		BindAddr: ":8080",
		LogLevel: "debug",
		DbURL:    "user=postgres password=mekannepes dbname=ethereum sslmode=disable",
		EthAddr:  "http://0.0.0.0:8545",
		keyStoreAddr:"C:/Users/mekannepes/Desktop/ethereum/rinkeby/keystore",
	}
	return c
}
