package server

import (
	"../Sql"
	"database/sql"
	_ "github.com/lib/pq"
)

func Start(config *Config) error {

	db, err := sql.Open("postgres", config.DbURL)
	if err != nil {
		return err
	}

	store := Sql.New(db)

	s, err := NewServer(config, store)
	if err != nil {
		return err
	}

	if err := s.router.Run(config.BindAddr); err != nil {
		return err
	}
	return nil
}
