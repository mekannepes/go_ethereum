package store

import (
	"../models"
)

type WalletRepository interface {
	Create(address string) error
	FindByAddress(address string) (int, error)
}

type TransactionRepository interface {
	Create(id int, txtHash string) error
	GetTransactions() ([]models.TransactionInfo, error)
}
