package store

type StoreRepositories interface {
	Wallet() WalletRepository
	Transaction() TransactionRepository
}
