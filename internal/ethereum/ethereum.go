package ethereum

import (
	"../models"
	"context"
	"crypto/ecdsa"
	"fmt"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"io/ioutil"
	"math"
	"math/big"
)

type Ethereum struct {
	ethClient *ethclient.Client
	ks        *keystore.KeyStore
}

func NewEthereum(clientAddr string, keyStoreAddr string) (*Ethereum, error) {
	ethereum := &Ethereum{}
	var err error
	ethereum.ethClient, err = ethclient.Dial(clientAddr)
	if err != nil {
		fmt.Println("Baglanmady")
		return nil, err
	}
	fmt.Println("Baglandy")
	ethereum.ks = keystore.NewKeyStore(keyStoreAddr, keystore.StandardScryptN, keystore.StandardScryptP)

	return ethereum, nil
}

//get all wallets
func (eth *Ethereum) GetWallets() []string {
	wallets := eth.ks.Accounts()

	var HexWallets []string

	for _, w := range wallets {
		HexWallets = append(HexWallets, w.Address.Hex())
	}

	return HexWallets

}

//get wallet balance
func (eth *Ethereum) GetBalance(address string) (*big.Float, error) {

	wallet := common.HexToAddress(address)

	balance, err := eth.ethClient.BalanceAt(context.Background(), wallet, nil)
	if err != nil {
		fmt.Println("Невозможно узнать баланс!")
		return nil, err
	}

	fBalance := new(big.Float)
	fBalance.SetString(balance.String())

	ethValue := new(big.Float).Quo(fBalance, big.NewFloat(math.Pow10(18)))

	return ethValue, nil
}

//create wallet
func (eth *Ethereum) CreateWallet(password string) (string, error) {
	wallet, err := eth.ks.NewAccount(password)
	if err != nil {
		fmt.Println("Невозможно создать кашелек")
		return "", err
	}

	return wallet.Address.Hex(), nil

}

//transfer ethereum
func (eth *Ethereum) Transfer(from, to, pwd string, ethvalue int64) (string, error) {

	wallets := eth.ks.Accounts()
	var index int

	for i, w := range wallets {
		if w.Address == common.HexToAddress(from) {
			index = i
			break
		}
	}

	jsonBytes, err := ioutil.ReadFile(wallets[index].URL.Path)
	if err != nil {
		return "", err
	}
	key, err := keystore.DecryptKey(jsonBytes, pwd)
	if err != nil {
		fmt.Println("Не правильный пароль")
		return "", err
	}

	privateKey := key.PrivateKey
	publicKey := privateKey.Public()

	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		fmt.Println("error casting public key to ECDSA")
		return "", err
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)

	nonce, err := eth.ethClient.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		fmt.Println("Problem with nonce")
		return "", err
	}
	value := big.NewInt(ethvalue) // in wei (1 eth)
	gasLimit := uint64(21000)

	gasPrice, err := eth.ethClient.SuggestGasPrice(context.Background())
	if err != nil {
		return "", err
	}

	toAddress := common.HexToAddress(to)
	var data []byte
	tx := types.NewTransaction(nonce, toAddress, value, gasLimit, gasPrice, data)

	chainId, err := eth.ethClient.ChainID(context.Background())
	if err != nil {
		return "", err
	}

	signedTx, err := types.SignTx(tx, types.NewEIP155Signer(chainId), privateKey)
	if err != nil {
		return "", err
	}

	err = eth.ethClient.SendTransaction(context.Background(), signedTx)
	if err != nil {
		fmt.Println("Невозможно отправить денги")
		return "", err
	}

	return signedTx.Hash().Hex(), nil

}

//get all last transactions
func (eth *Ethereum) GetLastTransactions(trans []models.TransactionInfo, address string) ([]models.TransactionInfo, error) {

	var transactions []models.TransactionInfo

	for _, t := range trans {
		m := models.TransactionInfo{}
		txHash := common.HexToHash(t.HashTransaction)
		tx, _, err := eth.ethClient.TransactionByHash(context.Background(), txHash)
		if err != nil {
			return nil, err
		}
		if address == tx.To().Hex()  ||  address == t.From {
			m.HashTransaction = tx.Hash().Hex()
			m.Value = tx.Value()
			m.Data = string(tx.Data())
			m.From = t.From
			m.To = tx.To().Hex()

			if address == t.From {
				m.Type = "Отправил"
			}else {
				m.Type = "Получил"
			}

			transactions = append(transactions, m)
		}
	}

	return transactions, nil
}
