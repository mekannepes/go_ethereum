package main

import (
	"../internal/server"
	"log"
)

func main()  {

	config := server.NewConfig()

	if err := server.Start(config); err!=nil  {
		log.Fatal(err)
	}

}
